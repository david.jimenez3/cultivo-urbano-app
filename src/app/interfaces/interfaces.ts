import { IonDatetime } from "@ionic/angular";

export interface Usuario {
  id?: number;
  name?: string;
  email?: string;
  password?: string;
  password_confirmation?: string;
}

export interface Dolencia {
  'identificador': number;
  'dolencia': string;
  'contenido': string;
  'mostrar': boolean;
}


export interface Planta {
  'id': number;
  'nombre': string;
  'imagen': string;
  'descripcion': string;
  'mostrar': boolean;
}

export interface Curso {
  'id': number;
  'titulo': string;
  'descripcion': string;
  'mostrar': boolean;
}

export interface Dispositivo{
  'id':number,
  'alias':string;  
}

export interface Leccion{
  'id':number;  
  'titulo':string;  
  'descripcion': string;
  
}

export interface RegistroDispositivo{
  'alias':string;
  'serial':string;
  'planta_id':Planta[];
}

export interface Medida{   
  'temperatura':number;
  'humedad':number;
  'ph':number;
  'tipo':string;
  'desfase_temperatura'?:number
  'desfase_ph'?:number;
  'desfase_humedad'?:number
  'fecha':string;
}