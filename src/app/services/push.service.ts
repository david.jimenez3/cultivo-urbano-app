import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';

@Injectable({
  providedIn: 'root'
})
export class PushService {

  constructor(private oneSignal: OneSignal) {}

  configuracionInicial(){
    this.oneSignal.startInit('45853b7c-e0f8-4131-9d71-328740d18fa9', '975403276694');    
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      // do something when notification is received
      console.log('NOtifiacion recibida',noti)
      
    });

    this.oneSignal.handleNotificationOpened().subscribe((noti) => {
      // do something when a notification is opened
      console.log('NOtifiacion abierta',noti)
    });

    this.oneSignal.endInit();
  }

}
