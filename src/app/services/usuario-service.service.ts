import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UsuarioServiceService {

  

  constructor(
    private storage: Storage,
    private navCtrl: NavController,
  ) { }
  
  logout() {    
    this.storage.clear();
    this.navCtrl.navigateRoot('/login', { animated: true });
  }

}
