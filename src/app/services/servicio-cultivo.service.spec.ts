import { TestBed } from '@angular/core/testing';

import { ServicioCultivoService } from './servicio-cultivo.service';

describe('ServicioCultivoService', () => {
  let service: ServicioCultivoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicioCultivoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
