import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NavController, Platform } from '@ionic/angular';
import { environment } from '../../environments/environment.prod';
import { HTTP } from '@ionic-native/http/ngx';
import { Storage } from '@ionic/storage';

const URL = environment.url

@Injectable({
  providedIn: 'root'
})
export class ServicioCultivoService {

  token: string = null;
  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  })

  constructor(
    private http: HttpClient,
    private storage: Storage,
    private navCtrl: NavController,
    private nativeHttp: HTTP,
    private plt:Platform,
   
  ) {
   
  }
  
  async guardarToken(token: string) {
    this.token = token;
    await this.storage.set('token', token);
  }

  async cargarToken() {
    this.token = await this.storage.get('token') || null;    
  }

  async agregarToken() {
    await this.cargarToken()
    this.headers.append('token', 'Bearer' + this.token)

  }

  async eliminarToken(){
    await this.storage.clear() 
  }

  createAuthorizationHeader(token) {
    return {
      headers: new HttpHeaders().set('Authorization', 'Bearer ' + token)
    }
  }

  async api_get(endpoint) {
    let url = URL.concat(endpoint)
    
    return new Promise(
      resolve => {
        this.storage.get('token').then(token => {
          let options = this.createAuthorizationHeader(token)
          this.http.get(url, options).subscribe((data) => {            
            if(data['code']==401){
              this.eliminarToken();
              this.navCtrl.navigateRoot('/login', { animated: true });
            }
            resolve(data);            
          }, error => {                
            resolve(error);
          });
        });
      });
  }

  api_post(endpoint, body) {
    let url = URL.concat(endpoint)    
    return new Promise(
      resolve => {
        this.storage.get('token').then(token => {
          let options = this.createAuthorizationHeader(token)
          this.http.post(url, body, options).subscribe(async data => {
            if(data['code']==401){
              this.eliminarToken();
              this.navCtrl.navigateRoot('/login', { animated: true });
            }
            if (data['token']) {
              await this.guardarToken(data['token']);
            }            
            resolve(data);
          }, error => {
            resolve(error);
          });
        });
      });
  }

  

  

}
