import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UiServiceService {

  loading: HTMLIonLoadingElement;

  constructor(private alertController: AlertController,
    private toastController: ToastController,
    private loadingCtrl: LoadingController) { }



  async alertaInformativa(message: string) {
    const alert = await this.alertController.create({
      message,
      buttons: ['OK']
    });

    await alert.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      position: 'top',
      duration: 1500
    });
    toast.present();
  }

  async presentLoading(message: string) {    
    this.loading = await this.loadingCtrl.create({
      message,
    });
    this.loading.present();
  }

  async stopLoading() {    
    await this.loading.dismiss();
  }

}
