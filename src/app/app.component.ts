import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { PushService } from './services/push.service';
import { Storage } from '@ionic/storage-angular';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform:Platform,
    private pushService:PushService,
    private storage: Storage,
    private navCtrl: NavController,
  ) {
    this.initializeApp();
  }
  token=false;
  initializeApp() {
    this.platform.ready().then(() => {     
      this.pushService.configuracionInicial();
    });
    this.storage.create();
    this.cargarToken()   

  }

  async cargarToken() {
    this.token = await this.storage.get('token') || null;    
    if ( !this.token ) {
      this.navCtrl.navigateRoot('/login');
      return Promise.resolve(false);
    }else{
      return this.navCtrl.navigateRoot('/tabs/dolencias');
    }
  }
}
