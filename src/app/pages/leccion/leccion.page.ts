import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { UiServiceService } from '../../services/ui-services.service';
import { Leccion } from '../../interfaces/interfaces';
@Component({
  selector: 'app-leccion',
  templateUrl: './leccion.page.html',
  styleUrls: ['./leccion.page.scss'],
})
export class LeccionPage implements OnInit {
  leccion: Leccion | null;
  constructor(
    private route: ActivatedRoute,
    private cultivoService: ServicioCultivoService,
    private uiService: UiServiceService
  ) { }

  ngOnInit() {
    
  }
  
  ionViewWillEnter() {
    let leccion_id = this.route.snapshot.paramMap.get('id')
    this.cagarLeccion(leccion_id);
  }

  ionViewWillLeave() {
    this.leccion =null
  }

  async cagarLeccion(leccion_id) {
    let query = `lecciones/${leccion_id}`
    this.uiService.presentLoading("Cargando lección");
    this.cultivoService.api_get(query).then(data => {
      if (data['code'] == 200) {
          
        this.leccion={
          id : data['data']['curso_id'],
          titulo : data['data']['titulo'],
          descripcion : data['data']['descripcion'],          
        }
        this.uiService.stopLoading();
      } else {
        this.uiService.stopLoading();
        this.uiService.alertaInformativa(data['message']);
      }

    });
  }
}
