import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DolenciasPageRoutingModule } from './dolencias-routing.module';

import { DolenciasPage } from './dolencias.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DolenciasPageRoutingModule
  ],
  declarations: [DolenciasPage],  
})
export class DolenciasPageModule {}
