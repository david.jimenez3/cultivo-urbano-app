import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DolenciasPage } from './dolencias.page';

const routes: Routes = [
  {
    path: '',
    component: DolenciasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DolenciasPageRoutingModule {}
