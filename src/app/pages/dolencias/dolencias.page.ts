import { Component, OnInit } from '@angular/core';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { Router } from '@angular/router';
import { UiServiceService } from '../../services/ui-services.service';
import { Dolencia } from '../../interfaces/interfaces';



@Component({
  selector: 'app-dolencias',
  templateUrl: './dolencias.page.html',
  styleUrls: ['./dolencias.page.scss'],
})
export class DolenciasPage implements OnInit {

  dolencias: Dolencia[] = [];


  public isMenuOpen: boolean = false;


  constructor(
    private cultivoService: ServicioCultivoService,
    private router: Router,
    private uiService: UiServiceService,
  ) { }



  ngOnInit() {
    
  }
  ionViewWillEnter (){    
    this.cagarDolencias()    
  }

  ionViewWillLeave(){
    this.dolencias=[];
  }
  

  /**
   * Allows the accordion state to be toggled (I.e. opened/closed)
   * @public
   * @method toggleAccordion
   * @returns {none}
   */
  toggleDetails(id) {

    this.dolencias.forEach((dolencia) => {
      if (dolencia.identificador == id) {
        dolencia.mostrar = true;
      } else {
        dolencia.mostrar = false;
      }
    });


  }

  async cagarDolencias() {
    this.uiService.presentLoading("Cargando lista de dolencias");
    this.cultivoService.api_get('dolencias').then(data => {      
      if (data['code'] == 200) {        
        let datos = data['data']['data'];
        datos.forEach(element => {
          this.dolencias.push(
            {
              identificador: element.identificador,
              dolencia: element.dolencia,
              contenido: element.contenido,
              mostrar: false
            });
        });
        this.uiService.stopLoading();
      } else {
        this.uiService.stopLoading();
        this.uiService.alertaInformativa('No hay conexion al servicio');
      }
    });

  }

  verPlantas(dolencia_id): void {
    this.router.navigate(['tabs/plantas', { id: dolencia_id }]);
  }



}
