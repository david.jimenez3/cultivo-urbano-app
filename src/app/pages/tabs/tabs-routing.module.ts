import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path:'',
    redirectTo:'/tabs/dolencias',
    pathMatch:'full'
  },
  {
    path: '',
    component: TabsPage,
    children:[
      {
        path:'dolencias',
        loadChildren:()=>import('../dolencias/dolencias.module').then(m=>m.DolenciasPageModule),
      },
      {
        path: 'plantas',
        loadChildren: () => import('../plantas/plantas.module').then( m => m.PlantasPageModule)
      },
      {
        path:'dispositivos',
        loadChildren:()=>import('../dispositivos/dispositivos.module').then(m=>m.DispositivosPageModule)
      } ,
      {
        path:'historial',
        loadChildren:()=>import('../historicos/historicos.module').then(m=>m.HistoricosPageModule)
      },
      
      {
        path: 'cursos',
        loadChildren: () => import('../cursos/cursos.module').then( m => m.CursosPageModule)
      },
      {
        path: 'lecciones',
        loadChildren: () => import('../lecciones/lecciones.module').then( m => m.LeccionesPageModule)
      },
      {
        path:'leccion',
        loadChildren:()=>import('../leccion/leccion.module').then(m=>m.LeccionPageModule)
      },
      {
        path: 'dispositivo-registrar',
        loadChildren: () => import('../dispositivo-registrar/dispositivo-registrar.module').then( m => m.DispositivoRegistrarPageModule)
      },
      {
        path: 'dispositivo-consultar',
        loadChildren: () => import('../dispositivo-consultar/dispositivo-consultar.module').then( m => m.DispositivoConsultarPageModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
