import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { UiServiceService } from '../../services/ui-services.service';
import { Dispositivo } from '../../interfaces/interfaces';

@Component({
  selector: 'app-dispositivos',
  templateUrl: './dispositivos.page.html',
  styleUrls: ['./dispositivos.page.scss'],
})
export class DispositivosPage implements OnInit {
  dispositivos:Dispositivo[]=[];
  constructor(
    private route: ActivatedRoute,
    private cultivoService: ServicioCultivoService,
    private router: Router,
    private uiService: UiServiceService
  ) { }

  ngOnInit() {
    
   

  }

  ionViewWillEnter (){
    let nuevo = this.route.snapshot.paramMap.get('nombre_dispositivo_nuevo')    
    this.cagarDispositivos()
    if (nuevo){
      this.uiService.alertaInformativa('se ha registrado su dispositivo'+nuevo);
    }
  }

  ionViewWillLeave(){
    this.dispositivos=[];
  }

  async cagarDispositivos() {
    let query = 'consultardispositivoregistrado'
    this.uiService.presentLoading("Cargando dispositivos");
    this.cultivoService.api_get(query).then(data => {      
      if (data['code'] == 200) {
        let datos = data['data']['data'];
        datos.forEach(element => {
          this.dispositivos.push(
            {
              id: element.dispositivo,
              alias: element.nombre,              
            });
        });
        this.uiService.stopLoading();
      } else {
        this.uiService.stopLoading();
        this.uiService.alertaInformativa(data['message']);
      }
    });
  }


  verDispositivo(dispositivo_id) {
    this.router.navigate(['/tabs/dispositivo-consultar', { dispositivo_id: dispositivo_id }]);   
    
  }

}
