import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { UiServiceService } from '../../services/ui-services.service';
import { Storage } from '@ionic/storage-angular';
import { Usuario } from '../../interfaces/interfaces';
import { Router } from '@angular/router';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  registerUser: Usuario = {
    email: 'david81ster@gmail.com',
    name: 'pepito pepita banana bananos',
    password: '123456',
    password_confirmation: '123456'
  };

  token = false;

  constructor(
    private cultivoService: ServicioCultivoService,
    private navCtrl: NavController,
    private uiService: UiServiceService,
    private storage: Storage,
    private router: Router,
  ) { }

  async ngOnInit() {
    this.storage.create();
    this.cargarToken()
  }

  async cargarToken() {

    this.token = await this.storage.get('token') || null;

    if (!this.token) {
      this.navCtrl.navigateRoot('/registro');
      return Promise.resolve(false);
    } else {
      return this.navCtrl.navigateRoot('/tabs/dolencias');
    }

  }

  async registro(fRegistro: NgForm) {
    let usuario: Usuario;
    usuario = {
      name: fRegistro.value.email,
      email: fRegistro.value.email,
      password: fRegistro.value.password,
      password_confirmation: fRegistro.value.password_confirmation
    }
    let endpoint='users'
    if (fRegistro.invalid) { return; }

    if (usuario.password != usuario.password_confirmation) {
      this.uiService.alertaInformativa('Contraseña no coincide.');
      return;
    }
    
    this.uiService.presentLoading("Por favor espere...");
      const valido = await this.cultivoService.api_post(endpoint,usuario);
      this.uiService.stopLoading();
      switch (valido['code']) {        
        case 200:
          this.navCtrl.navigateRoot('/login', { animated: true });
          this.uiService.alertaInformativa("Se ha enviado un email para confirmar el registro");
          break;
        case 422:
          this.uiService.alertaInformativa(valido['message']['email']);
          break;
        default:
          this.uiService.alertaInformativa("Lo sentimos, estamos trabajndo");
          break;
      }

  }

  login(){
    this.router.navigate(['login']);
  }

}
