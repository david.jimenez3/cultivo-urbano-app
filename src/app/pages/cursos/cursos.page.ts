import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { UiServiceService } from '../../services/ui-services.service';
import { Curso } from '../../interfaces/interfaces';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.page.html',
  styleUrls: ['./cursos.page.scss'],
})
export class CursosPage implements OnInit {
  cursos: Curso[] = [];
  constructor(
    private route: ActivatedRoute,
    private cultivoService: ServicioCultivoService,
    private router: Router,
    private uiService: UiServiceService
  ) { }

  ngOnInit() {

  }

  ionViewWillEnter() {
    let planta_id = this.route.snapshot.paramMap.get('id')
    this.cagarCursos(planta_id);
  }

  ionViewWillLeave() {
    this.cursos = [];
  }


  toggleDetails(id) {

    this.cursos.forEach((curso) => {
      if (curso.id == id) {
        curso.mostrar = true;
      } else {
        curso.mostrar = false;
      }
    });
  }
  async cagarCursos(planta_id) {
    let query = `cursos?planta=${planta_id}`
    this.uiService.presentLoading("Cargando lista de cursos");
    this.cultivoService.api_get(query).then(data => {

      if (data['code'] == 200) {

        let datos = data['data']['cursos'];
        datos.forEach(element => {

          this.cursos.push(
            {
              id: element.id,
              titulo: element.titulo,
              descripcion: element.descripcion,
              mostrar: false
            });

        });
        this.uiService.stopLoading();
      } else {
        this.uiService.stopLoading();
        this.uiService.alertaInformativa('Pronto volveremos... mil disculpas');
      }
    });




  }
  verLecciones(curso_id) {
    this.router.navigate(['tabs/lecciones', { id: curso_id }]);

  }

}
