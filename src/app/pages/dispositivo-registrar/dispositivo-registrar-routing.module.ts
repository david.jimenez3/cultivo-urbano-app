import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DispositivoRegistrarPage } from './dispositivo-registrar.page';

const routes: Routes = [
  {
    path: '',
    component: DispositivoRegistrarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DispositivoRegistrarPageRoutingModule {}
