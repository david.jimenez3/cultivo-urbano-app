import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DispositivoRegistrarPageRoutingModule } from './dispositivo-registrar-routing.module';

import { DispositivoRegistrarPage } from './dispositivo-registrar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DispositivoRegistrarPageRoutingModule
  ],
  declarations: [DispositivoRegistrarPage]
})
export class DispositivoRegistrarPageModule {}
