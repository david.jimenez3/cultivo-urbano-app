import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { UiServiceService } from '../../services/ui-services.service';
import { Planta ,RegistroDispositivo  } from '../../interfaces/interfaces';

@Component({
  selector: 'app-dispositivo-registrar',
  templateUrl: './dispositivo-registrar.page.html',
  styleUrls: ['./dispositivo-registrar.page.scss'],
})
export class DispositivoRegistrarPage implements OnInit {
  plantas: Planta[] = [];
  registrodispositivo: RegistroDispositivo = {
    alias: '',
    serial: '',
    planta_id: []
  }

  constructor(
    private cultivoService: ServicioCultivoService,
    private router: Router,
    private uiService: UiServiceService
  ) { }

  async ngOnInit() {
    
  }

  ionViewWillEnter (){    
    this.consultarPlantas()    
  }

  ionViewWillLeave(){
    this.plantas=[];
  }

  async consultarPlantas() {
    let query = 'consultar-planta'
    this.uiService.presentLoading("Cargando lista de plantas");
    this.cultivoService.api_get(query).then(data => {      
      if (data['code'] == 200) {
        let datos = data['data']['data'];
        datos.forEach(element => {
          this.plantas.push({
            id: element.identificador,
            nombre: element.planta,
            imagen: element.foto,
            descripcion: element.contenido,
            mostrar: false
          })
          
        });
        this.uiService.stopLoading();
      } else {
        this.uiService.stopLoading();
        this.uiService.alertaInformativa(data['message']);
      }

    });

  }

  async onSubmit(datos_registro: NgForm) {    
    
    let dispositivo={
      serial:datos_registro.value.serial,
      alias:datos_registro.value.alias,
    }    
    let query = `plantas/${datos_registro.value.planta_id}/registrardispositivos`
    this.uiService.presentLoading("Registrando...");
    this.cultivoService.api_post(query,dispositivo).then(data=>{      
      if (data['code'] == 200) {     
        this.router.navigate(['tabs/dispositivos', { nombre_dispositivo_nuevo: dispositivo.alias }]);   
        this.uiService.stopLoading();
      }else{
        this.uiService.stopLoading();
        this.uiService.alertaInformativa(data['message']);
      }

    })


  }
}
