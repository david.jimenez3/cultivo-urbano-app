import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { UiServiceService } from '../../services/ui-services.service';
import { Leccion } from '../../interfaces/interfaces';

@Component({
  selector: 'app-lecciones',
  templateUrl: './lecciones.page.html',
  styleUrls: ['./lecciones.page.scss'],
})
export class LeccionesPage implements OnInit {
  lecciones: Leccion[] = [];
  constructor(
    private route: ActivatedRoute,
    private cultivoService: ServicioCultivoService,
    private router: Router,
    private uiService: UiServiceService
  ) { }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    let curso_id = this.route.snapshot.paramMap.get('id');
    this.cagarLecciones(curso_id);
  }

  ionViewWillLeave() {
    this.lecciones = [];
  }

  verLeccion(leccion_id) {
    this.router.navigate(['tabs/leccion', { id: leccion_id }]);    

  }
  async cagarLecciones(curso_id) {
    let query = `lecciones?curso=${curso_id}`
    this.uiService.presentLoading("Cargando lista de lecciones");
    this.cultivoService.api_get(query).then(data => {
      if (data['code'] == 200) {      
        let datos = data['data']['lecciones'];        
        for (const element of datos) {
          this.lecciones.push(
            {
              id: element.id,
              titulo: element.titulo,             
              descripcion: element.descripcion,  
            });
        }
        this.uiService.stopLoading();
      } else {
        this.uiService.stopLoading();
        this.uiService.alertaInformativa(data['message']);
      }
    });
  }

  

}
