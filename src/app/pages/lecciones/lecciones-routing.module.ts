import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LeccionesPage } from './lecciones.page';

const routes: Routes = [
  {
    path: '',
    component: LeccionesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LeccionesPageRoutingModule {}
