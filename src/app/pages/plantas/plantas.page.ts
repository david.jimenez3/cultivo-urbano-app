import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,Router } from '@angular/router';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { UiServiceService } from '../../services/ui-services.service';
import { Planta } from '../../interfaces/interfaces';

@Component({
  selector: 'app-plantas',
  templateUrl: './plantas.page.html',
  styleUrls: ['./plantas.page.scss'],
})
export class PlantasPage implements OnInit {

  plantas: Planta[] = [];
  
  constructor(
    private route: ActivatedRoute,
    private cultivoService: ServicioCultivoService,
    private router: Router,
    private uiService: UiServiceService
  ) { }

  ngOnInit() {    
  }

  ionViewWillEnter (){    
    let dolencia_id = this.route.snapshot.paramMap.get('id')
    this.cagarPlantas(dolencia_id); 
  }

  ionViewWillLeave(){
    this.plantas=[];
  }

  async cagarPlantas(dolencia_id) {
    let query = `plantas?dolencia=${dolencia_id}`
    this.uiService.presentLoading("Cargando lista de plantas");
    this.cultivoService.api_get(query).then(data => {
     
      if (data['code'] ) {
        let datos = data['data']['plantas'];
        
        datos.forEach(element => {
          this.plantas.push(
            {
              id: element.id,
              nombre: element.nombre,
              imagen: element.imagen,
              descripcion: element.descripcion,
              mostrar: false
            });
        });
        this.uiService.stopLoading();
      } else {
        this.uiService.stopLoading();
        this.uiService.alertaInformativa(data['message']);        
      }
    });



  }

  toggleDetails(id) {

    this.plantas.forEach((curso) => {
      if (curso.id == id) {
        curso.mostrar = true;
      } else {
        curso.mostrar = false;
      }
    });


  }


  verCursos(planta_id) {

    this.router.navigate(['tabs/cursos', { id: planta_id }]);
  }

}
