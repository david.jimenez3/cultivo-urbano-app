import { Component, OnInit } from '@angular/core';

import { NavController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { UiServiceService } from '../../services/ui-services.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {



  loginUser = {
    email: 'david81ster@gmail.com',
    password: '123456'
  };

  constructor(
    private cultivoService: ServicioCultivoService,
    private navCtrl: NavController,
    private router: Router,
    private uiService: UiServiceService,

  ) { }

  async ngOnInit() {

  }



  async login(fLogin: NgForm) {

    if (fLogin.invalid) { return; }
    
    
    let usuario={
      email : fLogin.value.email,
      password : fLogin.value.password
    }
    let endpoint='login'
    this.uiService.presentLoading("Ingresando...");

    this.cultivoService.api_post(endpoint,usuario).then(data => {
      
      switch (data['code']) {
        case 200:
          this.navCtrl.navigateRoot('/tabs/dolencias', { animated: true });
          break;

        case 409:
          this.uiService.alertaInformativa(data['message']);
          break;

        default:
          this.uiService.alertaInformativa("Lo sentimos, estamos trabajando");
          break;
      }
      this.uiService.stopLoading();
    })
  }

  registro(){
    this.router.navigate(['registro']);
  }


}
