import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DispositivoConsultarPageRoutingModule } from './dispositivo-consultar-routing.module';

import { DispositivoConsultarPage } from './dispositivo-consultar.page';

import { ComponentsModule } from '../../components/components.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DispositivoConsultarPageRoutingModule,
    ComponentsModule
  ],
  declarations: [DispositivoConsultarPage]
})
export class DispositivoConsultarPageModule {}
