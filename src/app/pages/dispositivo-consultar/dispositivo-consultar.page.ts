import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,Router } from '@angular/router';
import { ServicioCultivoService } from '../../services/servicio-cultivo.service'
import { UiServiceService } from '../../services/ui-services.service';
import { Medida } from '../../interfaces/interfaces';


@Component({
  selector: 'app-dispositivo-consultar',
  templateUrl: './dispositivo-consultar.page.html',
  styleUrls: ['./dispositivo-consultar.page.scss'],
})
export class DispositivoConsultarPage implements OnInit {

  dispositivo_nombre : string='';
  dispositivo_id:number=0;
  medidas:Medida[]=[];
  

  constructor(
    private route: ActivatedRoute,
    private cultivoService: ServicioCultivoService,
    private uiService: UiServiceService,
    private router: Router,
  ) { }

  ngOnInit() {
    
  }

  ionViewWillEnter (){    
    this.dispositivo_id = +this.route.snapshot.paramMap.get('dispositivo_id')
    this.cargarMedidas()    
  }

  ionViewWillLeave(){
    this.medidas=[];
  }
  
  actualizarMedidas(){
    let dispositivo={
      dispositivo_id:this.dispositivo_id
    }
    let query ='consultardispositivo'
    this.uiService.presentLoading("Actualizando...");
    this.cultivoService.api_post(query,dispositivo).then(data => {      
      
      if (data['code'] == 200) {
        let datos = data['data'] ;                
        this.medidas.push(
          { 
            temperatura:datos.temperatura,
            humedad:datos.humedad,
            ph:datos.ph,
            tipo:datos.tipo,
            desfase_temperatura:datos.desfase_temperatura,
            desfase_ph:datos.desfase_ph,
            desfase_humedad:datos.desfase_humedad,
            fecha:datos.created_at
          });
          this.uiService.stopLoading();
      } else {
        this.uiService.stopLoading();
        this.uiService.alertaInformativa(data['message']);
      }
    });
  }

  cargarMedidas() {
    
    let query = `consultarhistorico?dispositivo_id=${this.dispositivo_id}`
    this.uiService.presentLoading("Consultando...");  
    this.cultivoService.api_get(query).then(data => {          
      if (data['code'] == 200) {
        let datos = data['data']['historicos'];
        this.dispositivo_nombre= data['data']['alias']
        datos.forEach(element => {
          this.medidas.push(
            {
              temperatura: element.temperatura,              
              humedad: element.humedad,              
              ph: element.ph, 
              tipo:element.tipo,
              desfase_temperatura:element.desfase_temperatura,
              desfase_ph:element.desfase_ph,
              desfase_humedad:element.desfase_humedad,  
              fecha:element.created_at
            });            
        });
        this.uiService.stopLoading();        
      } else {    
        this.uiService.stopLoading();       
        this.uiService.alertaInformativa(data['message']);    
        this.router.navigate(['tabs/dispositivos']);   
      }
    });
    
  }

  

}
