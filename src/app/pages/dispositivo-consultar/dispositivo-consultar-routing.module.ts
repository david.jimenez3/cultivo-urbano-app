import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DispositivoConsultarPage } from './dispositivo-consultar.page';

const routes: Routes = [
  {
    path: '',
    component: DispositivoConsultarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DispositivoConsultarPageRoutingModule {}
