import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-medidas',
  templateUrl: './medidas.component.html',
  styleUrls: ['./medidas.component.scss'],
})
export class MedidasComponent implements OnInit {

  @Input() temperatura: number;
  @Input() humedad: number;
  @Input() ph:number;
  @Input() desfase_ph:number;
  @Input() desfase_humedad:number;
  @Input() desfase_temperatura:number;
  @Input() tipo:string;
  @Input() fecha:string;
  constructor() { }

  ngOnInit() {}

 
}
