import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { HeaderComponent } from './header/header.component';
import { MedidasComponent } from './medidas/medidas.component';
@NgModule({
  declarations: [
    HeaderComponent,
    MedidasComponent
  ],  
  exports:[
    HeaderComponent,
    MedidasComponent
  ],imports: [
    CommonModule,
    IonicModule
  ]
})
export class ComponentsModule { }
